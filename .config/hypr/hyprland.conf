# █▀▀ █▄░█ █░█
# ██▄ █░▀█ ▀▄▀
env = GDK_BACKEND,wayland,x11  # Set gdk backend
env = QT_QPA_PLATFORM,wayland;xcb  # Set qt platform
env = QT_QPA_PLATFORMTHEME,qt6ct  # Set qt theme
env = XCURSOR_THEME,phinger-cursors-light  # Set cursor theme
env = XCURSOR_SIZE,24  # Set cursor size
env = MOZ_ENABLE_WAYLAND,1  # Enable wayland on firefox
env = SSH_AUTH_SOCK,$XDG_RUNTIME_DIR/gcr/ssh  # GCR ssh keyring
env = _JAVA_AWT_WM_NONREPARENTING,1  # Fix java wm bug
env = QT_WAYLAND_DISABLE_WINDOWDECORATION,1

# █▀▀ ▀▄▀ █▀▀ █▀▀
# ██▄ █░█ ██▄ █▄▄
exec			= killall waybar;			waybar
exec			= killall hyprpaper;	hyprpaper
exec			= killall wlsunset;		wlsunset -l 53.5 -L -2.3
exec-once = theming.sh # Theming script (gsettings, setcursor)
exec-once = /usr/lib/mate-polkit/polkit-mate-authentication-agent-1 # Polkit for root escalation
exec-once = swayidle # Idle dpms and suspend

# █▀▄▀█ █▀█ █▄░█ █ ▀█▀ █▀█ █▀█
# █░▀░█ █▄█ █░▀█ █ ░█░ █▄█ █▀▄
monitor = eDP-1,preferred,auto,1 # Set display to max res, auto offset

# ▀▄▀ █░█░█ ▄▀█ █▄█ █░░ ▄▀█ █▄░█ █▀▄
# █░█ ▀▄▀▄▀ █▀█ ░█░ █▄▄ █▀█ █░▀█ █▄▀
xwayland {
    force_zero_scaling = 1
  }

# █ █▄░█ █▀█ █░█ ▀█▀
# █ █░▀█ █▀▀ █▄█ ░█░
input {
  kb_layout = gb
  repeat_rate = 55 # Set characters to repeat on hold every 55ms
  repeat_delay = 375 # Set repeat timeout to 375ms
  follow_mouse = 2 # Follow mouse clicks for window focus
  sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
	# force_no_accel = 1
  float_switch_override_focus = 0 # Stop floating windows from stealing focus
  touchpad {
    natural_scroll = true
  }
}

device:tpps/2-elan-trackpoint {
	accel_profile = flat
}

device:razer-razer-viper-ultimate-dongle {
  accel_profile = flat
}

gestures {
  workspace_swipe = true
}

# █▀▀ █▀▀ █▄░█ █▀▀ █▀█ ▄▀█ █░░
# █▄█ ██▄ █░▀█ ██▄ █▀▄ █▀█ █▄▄
general {
  gaps_out=6 # Outer monitor gaps
  gaps_in=3 # Inner window gaps
  border_size=3 # Set window border width
  col.active_border=0xff89ddff 0xff7dcfff 0xff7aa2f7 270deg # Set active border colour (gradient)
  col.inactive_border=0xff414868 # Inactive gray
  no_border_on_floating = false # Reenables border on floating windows
  layout = dwindle # Set default layout
}

# █▀▄▀█ █ █▀ █▀▀
# █░▀░█ █ ▄█ █▄▄
misc {
  new_window_takes_over_fullscreen = 2
  disable_hyprland_logo = true # Disable logo on desktop
  disable_splash_rendering = true # Disable startup splashscreen
  animate_mouse_windowdragging = false # Disable windowdragging animations
  mouse_move_enables_dpms = true # Enables mouse move wakeup
  mouse_move_focuses_monitor = false # Disables hover for monitor focus
  focus_on_activate = true # Focusses windows which ask for activation (eg systray)
  enable_swallow = true # Enable window swalling
  swallow_regex = ^(foot)$ # Make foot swallow executed windows
  vfr = 1 # Variable framerate
  vrr = 1 # Variable refreshrate (freesync / gsync)
  # no_direct_scanout = false
  # render_ahead_of_time = true
  # render_ahead_safezone = 9
}

# █▀▄ █▀▀ █▀▀ █▀█ █▀█ ▄▀█ ▀█▀ █ █▀█ █▄░█
# █▄▀ ██▄ █▄▄ █▄█ █▀▄ █▀█ ░█░ █ █▄█ █░▀█
decoration {
    rounding = 6
    drop_shadow = true # Enable shadows on windows
    col.shadow = 0xee1a1a1a # Set shadow colour
    shadow_range = 5 # Set power range for shadow
    shadow_render_power = 5 # Set power / strength for shadow
    layerrule = blur, waybar # Enable blur on waybar
    layerrule = blur, wofi
    layerrule = blur, gtk-layer-shell
    layerrule = ignorezero, waybar
    layerrule = ignorezero, wofi
    layerrule = xray 1, gtk-layer-shell
    layerrule = xray 1, wofi
    blur {
        enabled = true
        size = 3
        passes = 3
        # noise = 0.075
        # brightness = 0.8
        # contrast = 0.8
      }
}

# ▄▀█ █▄░█ █ █▀▄▀█ ▄▀█ ▀█▀ █ █▀█ █▄░█ █▀
# █▀█ █░▀█ █ █░▀░█ █▀█ ░█░ █ █▄█ █░▀█ ▄█
animations {
    enabled = 1 # Enable animations

    bezier = overshot, 0.05, 0.9, 0.1, 1.05
    bezier = smoothOut, 0.36, 0, 0.66, -0.56
    bezier = smoothIn, 0.25, 1, 0.5, 1

    animation = windows, 1, 5, overshot, slide
    animation = windowsMove, 1, 4, default
    animation = border, 1, 10, default
    animation = fade, 1, 4, smoothIn
    animation = fadeDim, 1, 4, smoothOut
    animation = workspaces, 1, 6, default, slide
}

# █░░ ▄▀█ █▄█ █▀█ █░█ ▀█▀ █▀
# █▄▄ █▀█ ░█░ █▄█ █▄█ ░█░ ▄█
dwindle {
  no_gaps_when_only = false
  pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
  preserve_split = true # you probably want this
}

# █░█░█ █ █▄░█ █▀▄ █▀█ █░█░█   █▀█ █░█ █░░ █▀▀ █▀
# ▀▄▀▄▀ █ █░▀█ █▄▀ █▄█ ▀▄▀▄▀   █▀▄ █▄█ █▄▄ ██▄ ▄█
windowrule = float, file_progress
windowrule = float, confirm
windowrule = float, dialog
windowrule = float, download
windowrule = float, notification
windowrule = float, error
windowrule = float, splash
windowrule = float, confirmreset
windowrule = float, title:Open File
windowrule = float, title:branchdialog
windowrule = float, pavucontrol
windowrule = float, file-roller

windowrulev2 = idleinhibit focus, class:^(mpv)$
windowrulev2 = idleinhibit focus, class:^(atril)$
windowrulev2 = idleinhibit fullscreen, class:^(LibreWolf)$
windowrulev2 = idleinhibit fullscreen, class:^(com.stremio.stremio)$

# █▄▀ █▀▀ █▄█ █▄▄ █ █▄░█ █▀▄ █▀
# █░█ ██▄ ░█░ █▄█ █ █░▀█ █▄▀ ▄█
$mainMod = SUPER

# █▀ █▀▀ █▀█ █▀▀ █▀▀ █▄░█ █▀ █░█ █▀█ ▀█▀
# ▄█ █▄▄ █▀▄ ██▄ ██▄ █░▀█ ▄█ █▀█ █▄█ ░█░
bind = , Print, exec, screenshot.sh --monitor
bind = , XF86Launch2, exec, screenshot.sh --selection
bind = SHIFT, Print, exec, screenshot.sh --active

# █░█ █▀█ █░░ █░█ █▀▄▀█ █▀▀
# ▀▄▀ █▄█ █▄▄ █▄█ █░▀░█ ██▄
binde=, XF86AudioRaiseVolume, exec, audio.sh vol up 5
binde=, XF86AudioLowerVolume, exec, audio.sh vol down 5
binde=, XF86AudioMute, exec, audio.sh vol toggle
binde=, XF86AudioMicMute, exec, audio.sh mic toggle

# █▄▄ █▀█ █ █▀▀ █░█ ▀█▀ █▄░█ █▀▀ █▀ █▀
# █▄█ █▀▄ █ █▄█ █▀█ ░█░ █░▀█ ██▄ ▄█ ▄█
binde=, XF86MonBrightnessUp, exec, brightness.sh up 5
binde=, XF86MonBrightnessDown, exec, brightness.sh down 5

# ▄▀█ █▀█ █▀█ █▀
# █▀█ █▀▀ █▀▀ ▄█
bind = $mainMod, F, exec, thunar
bind = $mainMod, T, exec, foot
bind = $mainMod, B, exec, librewolf
bind = $mainMod SHIFT, P, exec, librewolf --private-window
bind = $mainMod, W, exec, vencord-desktop
bind = $mainMod, L, exec, swaylock
bind = $mainMod, D, exec, pkill wofi || wofi -c /home/faaris/.config/wofi/config -I -a
bind = $mainMod SHIFT, E, exec, pkill wlogout || wlogout --protocol layer-shell -b 5 -T 360 -B 360
bind = $mainMod SHIFT, R, exec, hyprctl reload

# █░█░█ █ █▄░█ █▀▄ █▀█ █░█░█   █▀▄▀█ ▄▀█ █▄░█ ▄▀█ █▀▀ █▀▄▀█ █▀▀ █▄░█ ▀█▀
# ▀▄▀▄▀ █ █░▀█ █▄▀ █▄█ ▀▄▀▄▀   █░▀░█ █▀█ █░▀█ █▀█ █▄█ █░▀░█ ██▄ █░▀█ ░█░
bind = $mainMod, Q, killactive
bind = $mainMod, Space, fullscreen
bind = $mainMod, Tab, togglefloating
bind = $mainMod, P, pseudo # dwindle
bind = $mainMod, S, togglesplit # dwindle

# █▀▀ █▀█ █▀▀ █░█ █▀
# █▀░ █▄█ █▄▄ █▄█ ▄█
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# █▀▄▀█ █▀█ █░█ █▀▀
# █░▀░█ █▄█ ▀▄▀ ██▄
bind = $mainMod SHIFT, left, movewindow, l
bind = $mainMod SHIFT, right, movewindow, r
bind = $mainMod SHIFT, up, movewindow, u
bind = $mainMod SHIFT, down, movewindow, d

# █▀█ █▀▀ █▀ █ ▀█ █▀▀
# █▀▄ ██▄ ▄█ █ █▄ ██▄
binde = $mainMod CTRL, left, resizeactive, -10 0
binde = $mainMod CTRL, right, resizeactive, 10 0
binde = $mainMod CTRL, up, resizeactive, 0 -10
binde = $mainMod CTRL, down, resizeactive, 0 10

# █▀ █░█░█ █ ▀█▀ █▀▀ █░█
# ▄█ ▀▄▀▄▀ █ ░█░ █▄▄ █▀█
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10
bind = $mainMod ALT, up, workspace, e+1
bind = $mainMod ALT, down, workspace, e-1

# █▀▄▀█ █▀█ █░█ █▀▀
# █░▀░█ █▄█ ▀▄▀ ██▄
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# █▀▄▀█ █▀█ █░█ █▀ █▀▀   █▄▄ █ █▄░█ █▀▄ █ █▄░█ █▀▀
# █░▀░█ █▄█ █▄█ ▄█ ██▄   █▄█ █ █░▀█ █▄▀ █ █░▀█ █▄█
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

bind=$mainMod SHIFT,N,submap,clean
submap=clean
bind=$mainMod SHIFT,N,submap,reset
submap=reset
