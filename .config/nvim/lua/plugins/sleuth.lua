return {
	-- Detect tabstop and shiftwidth automatically
	-- Note to self, never disable this
	{
		"tpope/vim-sleuth",
		event = "InsertEnter",
	},
}
