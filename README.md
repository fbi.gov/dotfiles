# ⚈ dotfiles

### 🔷 by fazzi.
### 🌃 based on the tokyonight color scheme.

<p align="center">
  <img src="assets/preview.png" alt="Rice Showcase">
</p>

- **Distribution:** [arch btw](https://archlinux.org/)
- **Compositor:** [Hyprland](https://github.com/hyprwm/Hyprland)
- **Terminal Emulator:** [foot](https://codeberg.org/dnkl/foot)
- **Status Bar:** [waybar](https://github.com/Alexays/Waybar/)
- **App Launcher:** [wofi](https://hg.sr.ht/~scoopta/wofi)
- **Notification Daemon:** [dunst](https://github.com/dunst-project/dunst)
- **Shell:** [zsh](https://www.zsh.org/)
- **Shell Plugin Manager:** [zgenom](https://github.com/jandamm/zgenom)
- **Browser:** [LibreWolf](https://librewolf.net/)

## 📁 Installation

```bash
git clone https://gitlab.com/fazzi/dotfiles.git
cd dotfiles
./install
```

This will use `dotbot` to set-up symlinks in the correct locations. It will also install zsh, paru, and other packages. **PLEASE BACKUP YOUR OWN CONFIGS BEFORE PROCEEDING!**

## 🖥️ Wallpapers

Browse through the collection of wallpapers in the [wallpaper folder](https://gitlab.com/fazzi/dotfiles/-/tree/hyprland-laptop/Wallpapers "wallpaper folder").
